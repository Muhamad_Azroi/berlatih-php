<html>
<head>
<title>Tentukan nilai</title>
</head>
<body>
    
<?php
function tentukan_nilai($number)
{
    if($number >= 85 && $number <= 100){
        echo "Nilai dengan $number <br>" ;
        echo "Sangat Baik <br>" ;
        echo "--------------------------- <br>";
    }
    else if($number >= 70 && $number < 85){
        
        echo "Nilai dengan $number <br>";
        echo "Baik <br>";
        echo "--------------------------- <br>";
    }
    else if($number >= 60 && $number < 70){
        
        echo "Nilai dengan $number <br>";
        echo "Cukup <br>";
        echo "--------------------------- <br>";
    }
    else{
        
        echo "Nilai dengan $number <br>";
        echo "Kurang <br>";
        echo "--------------------------- <br>";
    }
    

}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>

</body>

</html>